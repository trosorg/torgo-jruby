
module TorgoInterpreter

    class CodeBlock

        def addCommand()
        end

        def addInterpreterListener(listener)
        end

        def removeInterpreterListener(listener)
        end

        def process(scope)
        end

        def getCommands()
        end

        def isHalted()
        end

        def hasFunctions(name)
        end

        def addFunction(function)
        end

        def getParserRuleContext()
        end

        def hasVariable(name)
        end

        def getVariable(name)
        end

        def localVariables()
        end

        def getParent()
        end

    end

    class FunctionBlock < CodeBlock

        def getFunctionName()
        end

        def process(args=None)
        end

    end

    class Scope

        def initialize()
            @stack = []
            @listeners = []
        end

        def get(name)
        end

        def has(name)
        end

        def pop()
            return @stack.shift()
        end

        def peek(index = 0)
            return @stack[index]
        end

        def push(block)
            @stack.unshift(block)
        end

        def size()
            return @stack.length
        end

        def set(name, value)
        end

        def setNew(name, value)
        end

        def getFunction(name)
        end

        def hasFunction(name)
        end

        def localVariables()
        end

    end

    class DynamicScope < Scope
        def initialize()
            @scope = []
            super()
        end
    end

    class LexicalScope < Scope
        def initialize()
            @scope = []
            super()
        end
    end

end
